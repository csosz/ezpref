package com.csosz.ezpreferences

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson

/**
 * Created by csosz.andras on 2019-05-27
 */
interface EZPref {
    val sharedPreferences: SharedPreferences
}

val EZPref.gson: Gson by lazy {
    Gson()
}

internal inline fun SharedPreferences.edit(edits: SharedPreferences.Editor.() -> Unit) {
    val editor = edit()
    editor.edits()
    editor.apply()
}

/**
 * Be careful with nullName. You can override different object's same member's key
 */
abstract class SimplePref(
    context: Context,
    name: String? = null
) : EZPref {

    override val sharedPreferences: SharedPreferences = when (name) {
        null, "" -> PreferenceManager.getDefaultSharedPreferences(context)
        else -> context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    open fun clearAllData() {
        sharedPreferences.edit().clear().apply()
    }
}
