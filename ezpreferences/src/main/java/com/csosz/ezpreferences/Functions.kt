@file:Suppress("unused")

package com.csosz.ezpreferences

import androidx.lifecycle.MutableLiveData
import com.csosz.ezpreferences.delegate.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty

/**
 * Created by csosz.andras on 2019-05-27
 */

fun EZPref.booleanPref(
    key: String? = null,
    defaultValue: Boolean = false,
    async: Boolean = true,
    liveData: MutableLiveData<Boolean>? = null
): ReadWriteProperty<EZPref, Boolean> {
    return BooleanDelegate(key, defaultValue, async, liveData)
}

fun EZPref.floatPref(
    key: String? = null, defaultValue: Float = 0f,
    async: Boolean = true,
    liveData: MutableLiveData<Float>? = null
): ReadWriteProperty<EZPref, Float> {
    return FloatDelegate(key, defaultValue, async, liveData)
}

fun EZPref.intPref(
    key: String? = null, defaultValue: Int = 0,
    async: Boolean = true,
    liveData: MutableLiveData<Int>? = null
): ReadWriteProperty<EZPref, Int> {
    return IntDelegate(key, defaultValue, async, liveData)
}

fun EZPref.longPref(
    key: String? = null, defaultValue: Long = 0,
    async: Boolean = true,
    liveData: MutableLiveData<Long>? = null
): ReadWriteProperty<EZPref, Long> {
    return LongDelegate(key, defaultValue, async, liveData)
}

fun EZPref.stringPref(
    key: String? = null, defaultValue: String? = null,
    async: Boolean = true,
    liveData: MutableLiveData<String>? = null
): ReadWriteProperty<EZPref, String?> {
    return StringDelegate(key, defaultValue, async, liveData)
}

fun EZPref.stringReadOnlyPref(
    key: String? = null, initialize: String
): ReadOnlyProperty<EZPref, String> {
    return StringReadOnlyDelegate(key, initialize)
}

fun EZPref.stringSetPref(
    key: String? = null, defaultValue: Set<String>? = null,
    async: Boolean = true,
    liveData: MutableLiveData<Set<String>>? = null
): ReadWriteProperty<EZPref, Set<String>?> {
    return StringSetDelegate(key, defaultValue, async, liveData)
}

inline fun <reified T : Any> EZPref.gsonPref(
    key: String? = null,
    defaultValue: T? = null,
    liveData: MutableLiveData<T>? = null,
    type: Type = object : TypeToken<T>() {}.type
): ReadWriteProperty<EZPref, T?> {
    return GsonDelegate(key, defaultValue, type, liveData)
}