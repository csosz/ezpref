package com.csosz.ezpreferences.delegate

import com.csosz.ezpreferences.EZPref
import kotlin.reflect.KProperty

class StringReadOnlyDelegate(key: String? = null, private val initialize: String) :
    BaseReadOnlyDelegate<String>(key) {
    override fun getValue(thisRef: EZPref, property: KProperty<*>): String {
        val value = thisRef.sharedPreferences.getString(key(property), null)
        return value ?: thisRef.sharedPreferences.edit().putString(key(property), initialize)
            .apply().run {
                initialize
            }
    }
}