package com.csosz.ezpreferences.delegate

import androidx.lifecycle.MutableLiveData
import com.csosz.ezpreferences.EZPref
import kotlin.reflect.KProperty

class FloatDelegate(
    key: String? = null,
    private val defaultValue: Float = 0f,
    private val async: Boolean = true,
    private val liveData: MutableLiveData<Float>?
) : BaseDelegate<Float>(key) {

    override fun getValue(thisRef: EZPref, property: KProperty<*>): Float {
        return thisRef.sharedPreferences.getFloat(key(property), defaultValue)
    }

    override fun setValue(thisRef: EZPref, property: KProperty<*>, value: Float) {
        val editor = thisRef.sharedPreferences.edit().putFloat(key(property), value)
        if (async) editor.apply() else editor.commit()
        liveData?.postValue(value)
    }
}
