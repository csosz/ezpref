package com.csosz.ezpreferences.delegate

import com.csosz.ezpreferences.EZPref
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

abstract class BaseReadOnlyDelegate<T>(private val key: String?) :
    ReadOnlyProperty<EZPref, T> {
    protected fun key(property: KProperty<*>): String = key ?: property.name
}