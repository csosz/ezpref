package com.csosz.ezpreferences.delegate

import androidx.lifecycle.MutableLiveData
import com.csosz.ezpreferences.EZPref
import kotlin.reflect.KProperty

class BooleanDelegate(
    key: String? = null,
    private val defaultValue: Boolean = false,
    private val async: Boolean = true,
    private val liveData: MutableLiveData<Boolean>?
) : BaseDelegate<Boolean>(key) {

    override fun getValue(thisRef: EZPref, property: KProperty<*>): Boolean {
        return thisRef.sharedPreferences.getBoolean(key(property), defaultValue)
    }

    override fun setValue(thisRef: EZPref, property: KProperty<*>, value: Boolean) {
        val editor = thisRef.sharedPreferences.edit().putBoolean(key(property), value)
        if (async) editor.apply() else editor.commit()
        liveData?.postValue(value)
    }

}
