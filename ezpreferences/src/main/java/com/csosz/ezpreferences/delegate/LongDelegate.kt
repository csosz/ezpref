package com.csosz.ezpreferences.delegate

import androidx.lifecycle.MutableLiveData
import com.csosz.ezpreferences.EZPref
import kotlin.reflect.KProperty

class LongDelegate(
    key: String? = null,
    private val defaultValue: Long = 0,
    private val async: Boolean = true,
    val liveData: MutableLiveData<Long>?
) : BaseDelegate<Long>(key) {

    override fun getValue(thisRef: EZPref, property: KProperty<*>): Long {
        return thisRef.sharedPreferences.getLong(key(property), defaultValue)
    }

    override fun setValue(thisRef: EZPref, property: KProperty<*>, value: Long) {
        val editor = thisRef.sharedPreferences.edit().putLong(key(property), value)
        if (async) editor.apply() else editor.commit()
        liveData?.postValue(value)
    }
}