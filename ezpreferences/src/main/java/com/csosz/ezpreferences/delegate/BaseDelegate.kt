package com.csosz.ezpreferences.delegate

import com.csosz.ezpreferences.EZPref
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Created by csosz.andras on 2019-05-27
 */
abstract class BaseDelegate<T>(private val key: String?) : ReadWriteProperty<EZPref, T> {
    protected fun key(property: KProperty<*>): String = key ?: property.name
}