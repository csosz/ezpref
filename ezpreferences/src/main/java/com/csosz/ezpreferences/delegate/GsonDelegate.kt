package com.csosz.ezpreferences.delegate

import androidx.lifecycle.MutableLiveData
import com.csosz.ezpreferences.EZPref
import com.csosz.ezpreferences.edit
import com.csosz.ezpreferences.gson
import java.lang.reflect.Type
import kotlin.reflect.KProperty

@Suppress("SpellCheckingInspection")
class GsonDelegate<T : Any>(
    key: String? = null,
    private val defaultValue: T? = null,
    private val type: Type,
    private val liveData: MutableLiveData<T>?
) : BaseDelegate<T?>(key) {

    override fun getValue(thisRef: EZPref, property: KProperty<*>): T? {
        if (!thisRef.sharedPreferences.contains(key(property))) {
            defaultValue?.let { return it }
        }
        val string = thisRef.sharedPreferences.getString(key(property), null)
        string?.let {
            return thisRef.gson.fromJson(it, type)
        }
        return null
    }

    override fun setValue(thisRef: EZPref, property: KProperty<*>, value: T?) {
        thisRef.sharedPreferences.edit {
            putString(key(property), thisRef.gson.toJson(value))
        }
        liveData?.postValue(value)
    }
}
