package com.csosz.ezpreferences.delegate

import androidx.lifecycle.MutableLiveData
import com.csosz.ezpreferences.EZPref
import kotlin.reflect.KProperty

class StringSetDelegate constructor(
    key: String? = null,
    private val defaultValue: Set<String>? = null,
    private val async: Boolean = true,
    private val liveData: MutableLiveData<Set<String>>?
) : BaseDelegate<Set<String>?>(key) {

    override fun getValue(thisRef: EZPref, property: KProperty<*>): Set<String>? {
        return thisRef.sharedPreferences.getStringSet(key(property), defaultValue)
    }

    override fun setValue(thisRef: EZPref, property: KProperty<*>, value: Set<String>?) {
        val editor = thisRef.sharedPreferences.edit().putStringSet(key(property), value)
        if (async) editor.apply() else editor.commit()
        liveData?.postValue(value)
    }
}