package com.csosz.ezpreferences.delegate

import androidx.lifecycle.MutableLiveData
import com.csosz.ezpreferences.EZPref
import kotlin.reflect.KProperty

class IntDelegate(
    key: String? = null,
    private val defaultValue: Int = 0,
    private val async: Boolean = true,
    private val liveData: MutableLiveData<Int>?
) : BaseDelegate<Int>(key) {

    override fun getValue(thisRef: EZPref, property: KProperty<*>): Int {
        return thisRef.sharedPreferences.getInt(key(property), defaultValue)
    }

    override fun setValue(thisRef: EZPref, property: KProperty<*>, value: Int) {
        val editor = thisRef.sharedPreferences.edit().putInt(key(property), value)
        if (async) editor.apply() else editor.commit()
        liveData?.postValue(value)
    }
}